import netifaces

    

def getLanIpAddresses():
    results = []
    for net in netifaces.interfaces():
        #if (net.find('eth', 0, len(net))>=0) or (net.find('wlan', 0, len(net))>=0):
        if (net.find('eth', 0, len(net))>=0):
            links = netifaces.ifaddresses(net)
            if links.has_key(netifaces.AF_INET):
                for link in links[netifaces.AF_INET]:
                    results.append([net, link['addr']])                      
    
    return results

def getWlanIpAddresses():
    results = []
    for net in netifaces.interfaces():
        #if (net.find('eth', 0, len(net))>=0) or (net.find('wlan', 0, len(net))>=0):
        if (net.find('wlan', 0, len(net))>=0):
            links = netifaces.ifaddresses(net)
            if links.has_key(netifaces.AF_INET):
                for link in links[netifaces.AF_INET]:
                    results.append([net, link['addr']])                      
    
    return results
    

wlans = getWlanIpAddresses()
lans = getLanIpAddresses()

if len(lans)>0:
    serverIpAddress = lans[0][1]
elif len(wlans)>0:
    serverIpAddress = wlans[0][1]

print serverIpAddress







